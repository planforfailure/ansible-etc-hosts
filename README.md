# Description

Using templating to deploy a standard `/etc/hosts` 

- https://docs.ansible.com/ansible-core/devel/playbook_guide/playbooks_templating.html
- https://docs.ansible.com/ansible-core/devel/playbook_guide/playbooks_variables.html

***

## Dependencies
N/A

## Installation
Add supplemental host addresses as variables, otherwise you can run the playbook as below.

```bash
$ ansible-playbook deploy.yml -K
```

```yaml
---
- hosts: all
  become: yes
  gather_facts: yes
  roles:
    - roles/ansible-etc-hosts  
```
## Licence

MIT/BSD



